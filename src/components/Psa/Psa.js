import React from 'react';
import './Psa.css';

const Psa = () =>
  <div className="Psa">
    <h1>Hang on a second!</h1>

    <p>
      You were headed to the Daily Mail's website. Have a look at this
      cat gif instead. Refresh for another, if you like.
    </p>

    <p>
      Another selfless act performed just for you by
      <a href="http://twitter.com/Jonic">@Jonic</a>.
      You're <em>welcome</em>.
    </p>
  </div>
    
export default Psa;
