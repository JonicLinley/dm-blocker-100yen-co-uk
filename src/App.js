import React from 'react';

import Cat from './components/Cat/Cat'
import Psa from './components/Psa/Psa'

const App = () =>
  <div>
    <Psa />
    <Cat />
  </div>
    
export default App;
